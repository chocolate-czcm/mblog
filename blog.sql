/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50611
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2015-01-02 19:12:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` tinyint(6) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` varchar(120) NOT NULL,
  `keywords` varchar(120) NOT NULL,
  `description` varchar(300) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('17', '2015-01-02', '翻页测试', '翻页', '翻页测试内容', '<p>翻页测试翻页测试翻页测试翻页测试</p>');
INSERT INTO `article` VALUES ('16', '2015-01-02', '再来一次', '1234', '再来一次', '<p>1234<br/></p>');
INSERT INTO `article` VALUES ('7', '2014-12-29', 'WordPress博客主题TangStyle优化版1.1', '关键词1,关键词2', 'TangStyle优化版1.1在国庆的时候已大部分完成，拖延了发布时间，在这里说声抱歉，此次更新在1.0版本上功能更加完善和强大。请看下面详细介绍。', '<h3>TangStyle_Optimization1.1更新内容：</h3><ul><li><p>新增文章目录功能，默认为关闭状态（请配合标题3使用）</p></li><li><p>新增新浪前端库选项，有助于提高加载速度（默认关闭）</p></li><li><p>后台恢复友情链接管理选项（并只显示在首页）</p></li><li><p>添加站点地图页面模板（无需再安装站点地图插件）</p></li><li><p>添加控制首页摘要字数，正常情况下不建议修改此项，保持默认即可</p></li><li><p>默认添加百度分享按钮，若后台设置则以用户设置的分享按钮为准</p></li><li><p>添加CNZZ云推荐功能选项，让用户自行选择是否添加（去掉1.0版本的默认添加）</p></li><li><p>CSS样式表优化</p></li></ul><hr/><h3>TangStyle_Optimization1.0更新内容：</h3><ul><li><p>禁用谷歌字体</p></li><li><p>添加CNZZ云推荐（文章内容充足时才能显示）</p></li><li><p>添加面包屑导航</p></li><li><p>添加文章分页功能</p></li><li><p>增强了默认编辑器</p></li><li><p>默认添加百度分享按钮</p></li><li><p>添加文章浏览次数统计功能（免去一个插件）</p></li><li><p>首行缩进修改为2字符，默认为20px</p></li><li><p>管理员可查看每篇文章百度收录情况（代码由第四维提供）</p></li></ul><p><br/></p>');
INSERT INTO `article` VALUES ('6', '2014-12-29', 'WordPress博客主题TangStyle优化版1.1', '关键词1,关键词2', '描述内容太多', '<p>你好啊，我是邹修平</p>');
INSERT INTO `article` VALUES ('8', '2014-12-30', '博客疑似遭到360网站检测模拟攻击', '网站攻击', '就在上周将多说停用后使用了wordpress自带评论，在某一时刻突然遭到大量的垃圾留言，并且在不断尝试注入攻击（可惜wordpress本身很安全，注入攻击完全无效）。', '<p>攻击者昵称为:8888，邮箱为：hacker@hacker.org，攻击持续半个多小时，共收到50余条垃圾评论，并且都是注入攻击类型。一开始我以为是人为攻击，索性直接将IP拉黑，然后攻击停止。</p><p>后来通过与网友交流发现不仅是我一人遇到过此现象，原来都有类似情况，有朋友说是360的模拟攻击，检测网站是否存在漏洞，登录360站长工具查看了一下检测我网站的时间，果然时间完全吻合，那么是360的模拟攻击应该说得过去了。</p><p>但有个疑问是360显示检测我的网站仅耗费4分钟左右，模拟攻击却持续半个多小时（将IP拉黑后停止，如果不拉黑我也不知道攻击多久），你丫的\r\n360什么意思？而且还没通知我对我网站进行了模拟攻击，这个感觉非常不道德。难道是你360故意为之，然后让用户使用你的XXX网站卫士不成？</p><p>如果有遇到类似情况的朋友，建议立即采取保护措施，请查看看：<a href=\"http://www.xiaoz.me/archives/5242\" title=\"怎样有效的阻止WordPress垃圾评论\" target=\"_blank\">怎样有效的阻止WordPress垃圾评论</a>一文。</p><p><br/></p>');
INSERT INTO `article` VALUES ('9', '2014-12-30', '关于', '关于小z', '小z博客是一个以资源分享和技术教程为主的个人博客,站内提供各种优质资源、原创教程，以及小z的个人心得等。资源仅用于学习和交流，请勿用于其它用途！', '<blockquote><p><strong>关于本站：</strong></p></blockquote><p>小z博客是一个以资源分享和技术教程为主的个人博客,站内提供各种优质资源、原创教程，以及小z的个人心得等。资源仅用于学习和交流，请勿用于其它用途！</p><blockquote><p><strong>关于小z：</strong></p></blockquote><p>小z是一名在校大学生，平时喜欢折腾各种软件，经常关注互联网。故创建此博客希望能与网友分享一些实用的资源和自己的一些心得，一起学习和进步。</p><p>由于水平有限，网站还存在诸多不完善之处。因此我还需要您的宝贵建议来帮助我不断完善，若您有任何建议或需要帮助请在下面留言反馈或您可以<a href=\"http://www.xiaoz.me/go.php?http://weibo.com/zouxiupingwb/\" target=\"_blank\">关注我</a>的新浪微博及时了解网站最新动态，您的支持是我最大的动力。</p><blockquote><p><strong>友链申请：</strong></p></blockquote><p>同类型博客，每周至少更新一次，域名年龄&gt;=3个月，谷歌、百度收录正常，即可在下面留言申请。申请前请先做好本站链接,小z博客期待与您友链。</p><blockquote><p><strong>联系方式：</strong></p></blockquote><hr/><p>个人微信号:xiupingzou<br/><br/>Email：<a href=\"mailto:xiaozblog@163.com\">xiaozblog@163.com</a><br/><br/>QQ：337003006<br/><br/>站长交流群：147687134</p><p><br/></p>');
INSERT INTO `article` VALUES ('10', '2014-12-30', '小z博客免费网站服务', '博客捐助', '小z博客上线快一年的时间，认识了不少的朋友，也感谢大家对小z的支持，现在免费为小伙伴们提供wordpress网站服务，具体的服务内容如下：', '<p>小z博客上线快一年的时间，认识了不少的朋友，也感谢大家对小z的支持，现在免费为小伙伴们提供wordpress网站服务，具体的服务内容如下：</p><h3>服务内容：</h3><ul><li><p>WordPress站点速度优化</p></li><li><p>WordPress修改与调整</p></li><li><p>网站搬家</p></li><li><p>WordPress安装</p></li><li><p>Linux服务器环境配置</p></li><li><p>其它WordPress修改</p></li></ul><h3>部分案列用户：</h3><ul><li><p><a href=\"http://yurenty.com/\" title=\"宜兴紫砂\" rel=\"nofollow\" target=\"_blank\">宜兴紫砂</a> （网站搬家）</p></li><li><p><a href=\"http://www.jmfans.com/\" title=\"JustinMind中文基地\" rel=\"nofollow\" target=\"_blank\">JmFans</a> （域名更换）</p></li><li><p><a href=\"http://www.dssxs.com/\" title=\"沈工校园助手\" rel=\"nofollow\" target=\"_blank\">沈工校园助手</a> （微信托管）</p></li><li><p><a href=\"http://jlc-store.com\" title=\"立创电子\" rel=\"nofollow\" target=\"_blank\">立创电子</a> （头像缓存）</p></li></ul><p>我的QQ:337003006 &nbsp;&nbsp;&nbsp;&nbsp;邮箱：xiaozblog#163.com（请将#改为@），需要远程帮助的童鞋建议先<a href=\"http://www.xiaoz.me/archives/512\" title=\"TeamViewer V9.0.24482(远程控制)中文绿色版\" target=\"_blank\">下载Teamview软件</a>。</p><p>其它说明：我会尽最大的努力去帮助您解决问题，但是由于小z知识水平的局限，不敢保证所有问题一定能帮你解决，如果我不能解决的还请多多见谅。</p><p>当然了如果我的博客内容对你有所帮助，你可以进行自愿捐助，一张彩票、一包香烟，一顿饭你就能帮助小z博客不断持续的发展。</p><p><br/></p>');
INSERT INTO `article` VALUES ('11', '2014-12-30', 'WordPress 技巧：通过图片地址获取 ID', 'WordPress 技巧', '当我们知道图片的地址的时候，需要知道这张图片在数据库中的 ID，从而获取图片的详细信息，比如高，宽等等，来做一些操作，那么首先第一个怎么通过图片地址获取 ID？可以通过下面函数实现：', '<p>当我们知道图片的地址的时候，需要知道这张图片在数据库中的 ID，从而获取图片的详细信息，比如高，宽等等，来做一些操作，那么首先第一个怎么通过图片地址获取 ID？可以通过下面函数实现：</p><pre class=\" language-php\">function&nbsp;wpjam_get_attachment_id&nbsp;($img_url)&nbsp;{\r\n	$cache_key	=&nbsp;md5($img_url);\r\n	$post_id	=&nbsp;wp_cache_get($cache_key,&nbsp;&#39;wpjam_attachment_id&#39;&nbsp;);\r\n	if($post_id&nbsp;==&nbsp;false){\r\n\r\n		$attr		=&nbsp;wp_upload_dir();\r\n		$base_url	=&nbsp;$attr[&#39;baseurl&#39;].&quot;/&quot;;\r\n		$path&nbsp;=&nbsp;str_replace($base_url,&nbsp;&quot;&quot;,&nbsp;$img_url);\r\n		if($path){\r\n			global&nbsp;$wpdb;\r\n			$post_id	=&nbsp;$wpdb-&gt;get_var(&quot;SELECT&nbsp;post_id&nbsp;FROM&nbsp;$wpdb-&gt;postmeta&nbsp;WHERE&nbsp;meta_value&nbsp;=&nbsp;&#39;{$path}&#39;&quot;);\r\n			$post_id	=&nbsp;$post_id?$post_id:&#39;&#39;;&nbsp;\r\n		}else{\r\n			$post_id	=&nbsp;&#39;&#39;;\r\n		}\r\n\r\n		wp_cache_set(&nbsp;$cache_key,&nbsp;$post_id,&nbsp;&#39;wpjam_attachment_id&#39;,&nbsp;86400);\r\n	}\r\n	return&nbsp;$post_id;}</pre><p>标签：WordPress 技巧</p><p><br/></p>');
INSERT INTO `article` VALUES ('12', '2015-01-02', '香港主机推荐——老薛主机', '老薛主机', '折腾过VPS，折腾过虚拟主机，老薛算是其中一个。其实之前我并没有听说过，自从接触wordpress后很多朋友给我推荐了老薛，也用过一段时间，给我印象较深的是客服态度非常好。给正在建站而又不想备案的朋友推荐这个主机，下面说说我的使用感受。', '<p>折腾过VPS，折腾过虚拟主机，老薛算是其中一个。其实之前我并没有听说过，自从接触wordpress后很多朋友给我推荐了老薛，也用过一段时间，给我印象较深的是客服态度非常好。给正在建站而又不想备案的朋友推荐这个主机，下面说说我的使用感受。</p><p><br/></p><p>老薛的后台使用的在cPanel面板，这个面板的功能非常的丰富，而且官方提供了详细的使用教程，所以不用担心不会操作的问题。支持一键安装wordpress，如果还是不明白那也没事，可直接找客服免费安装就行，从服务态度上来说还是挺不错的。</p><p><br/></p><p>选择方面来说，建议选择香港主机，离大陆较近。因为我第一次选择的是美国2号主机（我的另一个网站B/S开发网当时在用），速度还是不错，不过遇到过宕机的情况，虽然时间只有几分钟，后来换了香港主机就没遇到过宕机的情况了。网站搭建完成后可以使用<a href=\"http://www.xiaoz.me/archives/2355\" title=\"网站监控你开启了吗？\" target=\"_blank\">网站监控</a>来检测网站状态。</p><p>说起备案就是个深深的痛啊，手里已经有三个域名了，我是个嫌麻烦的人，一个都还没备案。如果不想备案又要保证稳定的朋友，香港主机是个不错的选择。</p><blockquote><p><a href=\"http://www.xiaoz.me/jump.php?id=04\" title=\"老薛主机优惠码\" rel=\"nofollow\" target=\"_blank\">点此注册</a>并输入下面的优惠码享可受折扣，首次付款输入<span style=\"color:red;\">xiaoz20</span>享受20%优惠，输入<span style=\"color:red;\">xiaoz10</span>可享受终身10%的优惠，如果你是学生可以联系客服打8折，遗憾的是不能和优惠码同时使用，只能享受一个折扣。</p></blockquote><p><br/></p>');
INSERT INTO `article` VALUES ('13', '2014-12-30', '双旦福利，1分钱领取迅雷会员vip', '迅雷会员vip', '双旦来临，百度钱包与迅雷合作，使用百度钱包花费1分钱可领取1个月白金会员+15天影视VIP+15天网游加速器会员，亲测有效，有需要的小伙伴们可以试试。', '<h3 style=\"margin: 0px 0px 6px; padding: 8px 8px 8px 0px; font-weight: 400; text-shadow: rgb(238, 238, 238) 0px 1px 0px; width: auto; height: auto; line-height: 26px; text-indent: 28px; font-size: 18px; color: rgb(0, 102, 153);\"><span style=\"color: rgb(51, 51, 51); font-family: &#39;Microsoft YaHei&#39;, Tahoma, Arial; font-size: 14px; line-height: 26px; text-indent: 28px; background-color: rgb(255, 255, 255);\">双旦来临，百度钱包与迅雷合作，使用百度钱包花费1分钱可领取1个月白金会员+15天影视VIP+15天网游加速器会员，亲测有效，有需要的小伙伴们可以试试。</span></h3><h3 style=\"margin: 0px 0px 6px; padding: 8px 8px 8px 0px; font-weight: 400; text-shadow: rgb(238, 238, 238) 0px 1px 0px; width: auto; height: auto; line-height: 26px; text-indent: 28px; font-size: 18px; color: rgb(0, 102, 153);\">活动规则：</h3><ol style=\"padding: 0px; list-style: none; color: rgb(51, 51, 51); font-family: &#39;Microsoft YaHei&#39;, Tahoma, Arial; font-size: 14px; line-height: 21px; white-space: normal; background-color: rgb(255, 255, 255);\"><li><p>活动时间: 2014年10月14日至2014年12月31日；</p></li><li><p>仅限百度钱包新用户抢购1分钱1个月白金会员（此前在百度钱包账号内已绑定过银行卡的将被视为老用户，同一迅雷账号、百度账号、银行卡或银行预留手机号均视为同一用户）；</p></li><li><p>成功通过百度钱包快捷支付0.01元，立即获得1个月迅雷白金会员（即时直充到账）</p></li><li><p>双旦活动期间（12.24-12.31），参与本活动可额外获取15天影视VIP和15天网游加速器会员；</p></li><li><p>完成1分钱支付后，百度钱包帐上将获得500点百赚分，100点百赚分=1元钱，可在下次使用百度钱包快捷支付时候使用百赚分进行抵现；</p></li></ol><blockquote style=\"margin: 10px 0px; padding: 10px; border: 1px dashed rgb(221, 221, 221); color: rgb(51, 51, 51); font-family: &#39;Microsoft YaHei&#39;, Tahoma, Arial; font-size: 14px; line-height: 21px; white-space: normal; background: rgb(241, 241, 241);\"><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 26px;\"><span style=\"font-weight: 600;\">附：</span><a href=\"http://act.vip.xunlei.com/vip/2014/baidu04/?uid=306282896\" title=\"领取迅雷VIP\" rel=\"nofollow\" target=\"_blank\" style=\"color: rgb(0, 68, 153); text-decoration: none;\">活动地址</a></p></blockquote><p><br/></p>');
INSERT INTO `article` VALUES ('14', '2015-01-01', ' 浅析php header 跳转吧', 'PHP', 'PHP的header函数 可以很少代码就能实现HTML代码中META 标签', '<p>PHP的header函数 可以很少代码就能实现HTML代码中META 标签<br/><strong>这里只说用 header函数来做页面的跳转</strong></p><p><strong><span style=\"COLOR: #ff0000\">1. HTML代码中页面的跳转的代码<br/></span></strong>HTML meta refresh 刷新与跳转(重定向)页面<br/>refresh 属性值 -- 刷新与跳转(重定向)页面<br/>* refresh用于刷新与跳转(重定向)页面<br/>* refresh出现在http-equiv属性中，使用content属性表示刷新或跳转的开始时间与跳转的网址<br/>* 引用网址:http://www.dreamdu.com/xhtml/refresh/</p><p><strong>meta refresh示例<br/></strong>5秒之后刷新本页面:<br/><meta http-equiv=\"refresh\" content=\"5\"/><br/>5秒之后转到梦之都首页:<br/><meta http-equiv=\"refresh\" content=\"5; url=http://www.jb51.net\"/></p><p><strong><span style=\"COLOR: #ff0000\">2. PHP中使用header函数<br/></span></strong>上面已经了解了HTML中写跳转(重定向)页面的代码<br/>php代码示例<br/>header(&#39;Refresh: 3; url=http://www.jb51.net&#39;);</p><p><br/></p>');
INSERT INTO `article` VALUES ('15', '2015-01-02', '新浪博客', '新浪', '新浪博客', '<span style=\"font-size: 14px;\">我的新浪博客</span><p><br/></p>');
INSERT INTO `article` VALUES ('19', '2015-01-02', '测试缩进和图片', '测试', '图片和排版测试', '<p>你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你</p><p>好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是</p><p>你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是你好啊，这是</p><p><br/></p><p style=\"text-align:center\"><img src=\"/ueditor/php/upload/image/20150102/1420192833113634.jpg\" title=\"1420192833113634.jpg\" alt=\"weixin.jpg\"/></p>');

-- ----------------------------
-- Table structure for seo
-- ----------------------------
DROP TABLE IF EXISTS `seo`;
CREATE TABLE `seo` (
  `title` char(80) NOT NULL,
  `keywords` char(100) NOT NULL,
  `description` char(200) NOT NULL,
  `subtitle` char(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seo
-- ----------------------------
INSERT INTO `seo` VALUES ('邹修平Blog', '邹修平', '由小z开发的轻博客系统M-Blog开始公测，你可曾听说过M-Blog，这不科学啊。', '路漫漫其修远兮，吾将上下而求索。');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` tinyint(6) NOT NULL AUTO_INCREMENT,
  `account` char(12) NOT NULL,
  `nickname` char(20) NOT NULL,
  `email` char(40) NOT NULL,
  `pass` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '管理员', 'xiaozblog@163.com', 'dec24e1021acdead57402ac9c367dfe2');
